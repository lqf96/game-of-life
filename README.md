# [Conway's Game of Life Demo](https://lqf96.coding.me/game-of-life)
This is a website demonstrating Conway's Game of Life.  
See assignment report [here](REPORT.md).

## License
[GPLv3](LICENSE)
