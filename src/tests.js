//Tests.js: Game-of-life tests
import GameOfLife from "gol/game-of-life";

//Assertion
function assert(cond)
{
    if (!cond)
        throw new Error("Assertion failed");
}

//Game controller test
describe("Game-of-Life Game Controller", () => {
    let update_ui = false;
    //New game controller
    let gol_ctrler = new GameOfLife({
        width: 10,
        height: 10,
        paint_func: () => {
            update_ui = true;
        }
    });

    //Function "at()"
    it("at() should return cell status at given position.", () => {
        //Dead test
        assert(gol_ctrler.at(0, 0)==0);
        //Live test
        gol_ctrler.game_area[0][0] = 1;
        assert(gol_ctrler.at(0, 0)==1);
    });

    //Function "set()"
    it("set() should set status of the cell and given position and update the UI.", () => {
        //Live test
        update_ui = false;
        gol_ctrler.set(0, 0, 0);
        assert(gol_ctrler.game_area[0][0]==0);
        assert(update_ui);
        //Dead test
        update_ui = false;
        gol_ctrler.set(0, 0, 1);
        assert(gol_ctrler.game_area[0][0]==1);
        assert(update_ui);
    });

    //Function "iterate()"
    it("iterate() should obey game-of-life rules.", () => {
        function live_or_dead(n_live, n_limit)
        {
            return (n_live>n_limit)?1:0;
        }

        for (let k=8;k>=0;k--)
        {
            //Set surrounding cells
            gol_ctrler.game_area[0][0] = live_or_dead(k, 0);
            gol_ctrler.game_area[0][1] = live_or_dead(k, 1);
            gol_ctrler.game_area[0][2] = live_or_dead(k, 2);
            gol_ctrler.game_area[1][0] = live_or_dead(k, 3);
            gol_ctrler.game_area[1][2] = live_or_dead(k, 4);
            gol_ctrler.game_area[2][0] = live_or_dead(k, 5);
            gol_ctrler.game_area[2][1] = live_or_dead(k, 6);
            gol_ctrler.game_area[2][2] = live_or_dead(k, 7);

            //Iterate
            gol_ctrler.iterate();
            //Test
            if ((k==2)||(k==3))
                assert(gol_ctrler.game_area[1][1]==1);
            else
                assert(gol_ctrler.game_area[1][1]==0);
        }
    });

    //Function "random()"
    it("random() should create an evenly distributed random layout.", () => {
        gol_ctrler.random();
        let n_live = 0, n_dead = 0;

        for (let i=0;i<10;i++)
            for (let j=0;j<10;j++)
                if (gol_ctrler.game_area[i][j]==1)
                    n_live++;
                else
                    n_dead++;

        console.log(`${n_live} live cells.`);
        console.log(`${n_dead} dead cells.`);
        let diff = Math.abs(n_live-n_dead);
        console.log(`Difference: ${diff}.`);
    });

    //Function "clear()"
    it("clear() should clear the game area.", () => {
        gol_ctrler.random();
        gol_ctrler.clear();
        for (let i=0;i<10;i++)
            for (let j=0;j<10;j++)
                assert(gol_ctrler.game_area[i][j]==0);
    });
});
