//Index.js: Game-of-life program entry
import $ from "jquery";
import GameOfLife from "./game-of-life";

//Initialization
$(() => {
    //Configurations
    const GOL_WIDTH = 80,
        GOL_HEIGHT = 80,
        GOL_FALSE_COLOR = "white",
        GOL_TRUE_COLOR = "black";

    //Game of life controller
    let gol_ctrler = new GameOfLife({
        width: GOL_WIDTH,
        height: GOL_HEIGHT,
        paint_func: (x, y, status) => {
            let color = (status==1)?GOL_TRUE_COLOR:GOL_FALSE_COLOR;
            $(`#gol-${x}-${y}`).css("background-color", color);
        }
    });

    //Game of life UI
    let gol_table = $("#gol");
    for (let i=0;i<GOL_HEIGHT;i++)
    {
        let new_line = $("<tr />");
        for (let j=0;j<GOL_WIDTH;j++)
        {
            let cell = $("<td />")
                .attr("id", `gol-${j}-${i}`)
                .on("click", () => {
                    gol_ctrler.set(j, i, 1-gol_ctrler.at(j, i));
                });
            new_line.append(cell);
        }
        gol_table.append(new_line);
    }

    //Oneshot button
    $("#btn-oneshot").on("click", () => {
        gol_ctrler.iterate();
    });

    //Clear button
    $("#btn-clear").on("click", () => {
        gol_ctrler.clear();
    });

    //Running status
    let running_status = false;
    //Runner handle
    let runner_handle = null;

    //Run / stop button
    $("#btn-run-stop").on("click", function()
    {
        running_status = !running_status;

        //Run game of life
        if (running_status)
        {
            //FPS
            let fps = parseInt($("#speed-input").val());

            //Start runner
            runner_handle = setInterval(() => {
                let changed_count = gol_ctrler.iterate();
                //Nothing changed, stop runner
                if (changed_count==0)
                    $(this).click();
            }, 1000/fps);

            //Set button text
            $(this).text("Stop");

            //Disable oneshot button and speed input
            $("#btn-oneshot").prop("disabled", true);
            $("#speed-input").prop("disabled", true);
        }
        //Stop game of life
        else
        {
            //Stop runner
            clearInterval(runner_handle);

            //Set button text
            $(this).text("Run");

            //Enable oneshot button and speed input
            $("#btn-oneshot").prop("disabled", false);
            $("#speed-input").prop("disabled", false);
        }
    });

    //Random button
    $("#btn-random").on("click", () => {
        gol_ctrler.random();
    });
});
