"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

//Game-of-life.js: Game of life game controller class

//Dummy function
function dummy_func() {}

//Game of life class

var GameOfLife = function () {
    //Constructor
    function GameOfLife(config) {
        _classCallCheck(this, GameOfLife);

        //Default parameters
        this.width = config.width || 50;
        this.height = config.height || 50;
        this.paint_func = config.paint_func || dummy_func;

        //Game area and temporary area
        this.game_area = new Array(config.width);
        this.temp_area = new Array(config.width);
        for (var i = 0; i < config.width; i++) {
            this.game_area[i] = new Array(config.height);
            this.temp_area[i] = new Array(config.height);
            for (var j = 0; j < config.height; j++) {
                this.game_area[i][j] = this.temp_area[i][j] = 0;
            }
        }
    }

    //Get cell status at given position


    _createClass(GameOfLife, [{
        key: "at",
        value: function at(x, y) {
            return this.game_area[x][y];
        }

        //Set cell status at given position

    }, {
        key: "set",
        value: function set(x, y, status) {
            //Illegal input
            if (status != 0 && status != 1) throw new Error("Illegal cell status");

            this.game_area[x][y] = status;
            //Update UI
            this.paint_func(x, y, status);
        }

        //Calculate and apply the status of cells at next round

    }, {
        key: "iterate",
        value: function iterate() {
            //Amount of cells whose status changed
            var changed_count = 0;

            //Update each cell
            for (var i = 0; i < this.width; i++) {
                for (var j = 0; j < this.height; j++) {
                    //Surrounding position
                    var left = i == 0 ? this.width - 1 : i - 1,
                        right = i == this.width - 1 ? 0 : i + 1,
                        up = j == 0 ? this.height - 1 : j - 1,
                        down = j == this.height - 1 ? 0 : j + 1;
                    //Amount of surrounding cells alive
                    var amount = this.game_area[left][up] + this.game_area[i][up] + this.game_area[right][up] + this.game_area[left][j] + this.game_area[right][j] + this.game_area[left][down] + this.game_area[i][down] + this.game_area[right][down];

                    //Rules
                    if (amount == 3) this.temp_area[i][j] = 1;else if (amount == 2) this.temp_area[i][j] = this.game_area[i][j];else this.temp_area[i][j] = 0;
                    //Update changed count and repaint cell
                    if (this.temp_area[i][j] != this.game_area[i][j]) {
                        this.paint_func(i, j, this.temp_area[i][j]);
                        changed_count++;
                    }
                }
            }

            //Swap game area and temporary area
            var new_game_area = this.temp_area;
            this.temp_area = this.game_area;
            this.game_area = new_game_area;

            return changed_count;
        }

        //Clear game area

    }, {
        key: "clear",
        value: function clear() {
            for (var i = 0; i < this.width; i++) {
                for (var j = 0; j < this.height; j++) {
                    this.set(i, j, 0);
                }
            }
        }

        //Generate a random layout

    }, {
        key: "random",
        value: function random() {
            for (var i = 0; i < this.width; i++) {
                for (var j = 0; j < this.height; j++) {
                    this.set(i, j, Math.round(Math.random()));
                }
            }
        }
    }]);

    return GameOfLife;
}();

exports.default = GameOfLife;