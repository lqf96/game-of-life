"use strict";

var _jquery = require("jquery");

var _jquery2 = _interopRequireDefault(_jquery);

var _gameOfLife = require("./game-of-life");

var _gameOfLife2 = _interopRequireDefault(_gameOfLife);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//Initialization
//Index.js: Game-of-life program entry
(0, _jquery2.default)(function () {
    //Configurations
    var GOL_WIDTH = 80,
        GOL_HEIGHT = 80,
        GOL_FALSE_COLOR = "white",
        GOL_TRUE_COLOR = "black";

    //Game of life controller
    var gol_ctrler = new _gameOfLife2.default({
        width: GOL_WIDTH,
        height: GOL_HEIGHT,
        paint_func: function paint_func(x, y, status) {
            var color = status == 1 ? GOL_TRUE_COLOR : GOL_FALSE_COLOR;
            (0, _jquery2.default)("#gol-" + x + "-" + y).css("background-color", color);
        }
    });

    //Game of life UI
    var gol_table = (0, _jquery2.default)("#gol");

    var _loop = function _loop(i) {
        var new_line = (0, _jquery2.default)("<tr />");

        var _loop2 = function _loop2(j) {
            var cell = (0, _jquery2.default)("<td />").attr("id", "gol-" + j + "-" + i).on("click", function () {
                gol_ctrler.set(j, i, 1 - gol_ctrler.at(j, i));
            });
            new_line.append(cell);
        };

        for (var j = 0; j < GOL_WIDTH; j++) {
            _loop2(j);
        }
        gol_table.append(new_line);
    };

    for (var i = 0; i < GOL_HEIGHT; i++) {
        _loop(i);
    }

    //Oneshot button
    (0, _jquery2.default)("#btn-oneshot").on("click", function () {
        gol_ctrler.iterate();
    });

    //Clear button
    (0, _jquery2.default)("#btn-clear").on("click", function () {
        gol_ctrler.clear();
    });

    //Running status
    var running_status = false;
    //Runner handle
    var runner_handle = null;

    //Run / stop button
    (0, _jquery2.default)("#btn-run-stop").on("click", function () {
        var _this = this;

        running_status = !running_status;

        //Run game of life
        if (running_status) {
            //FPS
            var fps = parseInt((0, _jquery2.default)("#speed-input").val());

            //Start runner
            runner_handle = setInterval(function () {
                var changed_count = gol_ctrler.iterate();
                //Nothing changed, stop runner
                if (changed_count == 0) (0, _jquery2.default)(_this).click();
            }, 1000 / fps);

            //Set button text
            (0, _jquery2.default)(this).text("Stop");

            //Disable oneshot button and speed input
            (0, _jquery2.default)("#btn-oneshot").prop("disabled", true);
            (0, _jquery2.default)("#speed-input").prop("disabled", true);
        }
        //Stop game of life
        else {
                //Stop runner
                clearInterval(runner_handle);

                //Set button text
                (0, _jquery2.default)(this).text("Run");

                //Enable oneshot button and speed input
                (0, _jquery2.default)("#btn-oneshot").prop("disabled", false);
                (0, _jquery2.default)("#speed-input").prop("disabled", false);
            }
    });

    //Random button
    (0, _jquery2.default)("#btn-random").on("click", function () {
        gol_ctrler.random();
    });
});