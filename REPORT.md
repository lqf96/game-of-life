# Conway's Game of Life Demo
Qifan Lu - 2014013423 - lqf.1996121@gmail.com

## Features
* Click the cell to make it dead or alive
* Generate a random cell layout
* Clear all cells
* Run the game frame by frame or consecutively
* Adjust game speed with mouse

## How to run this project?
* `cd` into project directory and run `npm install` to install all dependencies.
* Run `npm start` and browse Game of Life at http://127.0.0.1:8080/.
* Browse http://127.0.0.1:8080/tests.html to run unit tests.
* Run `npm run lint` to see ESLint result.
* Run `npm run debug` to debug Game of Life. Babel will keep recompiling ES6 javascript at background.

## Project Details
* Project repository: https://coding.net/u/lqf96/p/game-of-life/git  
  Project demo: https://lqf96.coding.me/game-of-life  
  Unit tests: https://lqf96.coding.me/game-of-life/tests.html
* Environment:
  - Hardware: Macbook Pro Early 2015
  - OS: macOS Sierra 10.12
  - Javascript Environment: NodeJS v6.5.0
* 3rd party libraries:
  - `babel-cli` @ 6.14.0
  - `babel-preset-es2015` @ 6.14.0
  - `concurrently` @ 2.2.0
  - `eslint` @ 3.5.0
  - `http-server` @ 0.9.0
  - `mocha` @ 3.1.0
* Project structure:
  - `.babelrc`: Babel ES6 transpile settings
  - `.eslintrc.json`: ESLint settings
  - `index.css`: Game-of-life stylesheets
  - `index.html`: Game-of-life web page (Site entry)
  - `js`: Transpiled Javascript code (See `src` for files)
  - `package.json`: Node.js package information
  - `src`: Game-of-life source code
    + `game-of-life.js`: Game-of-life game controller
    + `index.js`: Game UI codes
    + `tests.js`: Game-of-life controller unit tests
  - `tests.html`: Unit tests page

## Game-of-life Controller API
ES6 class `GameOfLife` is responsible for the logic of the game, and is decoupled from game UI. You can find it in `src/game-of-life.js`.  
* Class `GameOfLife`
  - `GameOfLife(config)`: Create a game-of-life controller with a configuration object, keys of which can be:
    + `width`: The width of the game area.
    + `height`: The height of the game area.
    + `paint_func`: A function called when game area is updated. Takes `x`, `y` and `status` parameters.
  - `at(x, y)`: Get cell status at given position.
  - `set(x, y, status)`: Set cell status at given position. Will call `paint_func` to update game UI.
  - `iterate()`: Calculate and apply the status of cells at next round. Will call `paint_func` to update game UI. Use dual buffer painting. (See below for details)
  - `clear()`: Clear the game area.

## Test Cases
* `at()` should return cell status at given position.
  - Check if `at(0, 0)` returns "dead".
  - Manually set `(0, 0)` to 1 and check if `at(0, 1)` returns "alive".
* `set()` should set status of the cell and given position and update the UI.
  - Set `(0, 0)` to "dead" and manually check status at `(0, 0)`.
  - Set `(0, 0)` to "alive" and manually check status at `(0, 0)`.
* `iterate()` should obey game-of-life rules.
  - Set amount of alive surrounding cells to 0 to 8, and examine the status of central cell at next tick.
* `random()` should create an evenly distributed random layout.
  - Call `random()` to create a random layout, and calculate amount of cells dead or alive. Display statistics in the console.
* `clear()` should clear the game area.
  - Call `clear()` and check if every cell is "dead".

## Problems, Thoughts & Suggestions
* UI testing: When game code is spilt into controller and UI code, it is relatively easy to test game controller code and game logic. But what about UI? Should we use some sort of UI testing framework, and should we show the test result on the screen and judge it manually?
* Dual buffer painting: In game-of-life, the status of cell at next tick is determined by status of surrounding cells at this tick. So extra memory is needed to hold the status of all cells at next tick. Since allocating memory at each tick is unefficient and seems like a bad idea, I come up with what I called "dual buffer painting": allocating two pieces of memory, each of which can hold the status of all cells. At each moment we have one of them recording the status of all cells, and use another one to hold the status of cells at next tick when calculating. Then we simply swap the two matrixes and compare them, and update the UI.
